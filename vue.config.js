const path = require('path')
const ROOT = path.resolve(__dirname)

module.exports = {
  chainWebpack: config => {
    config.resolve.alias
      .set('@views', `${ROOT}/src/views`)
      .set('@stylesheets', `${ROOT}/src/stylesheets`)
      .set('@store', `${ROOT}/src/store`)
      .set('@components', `${ROOT}/src/components`)
  },
  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: [path.resolve(__dirname, './src/stylesheets/variables.scss')],
    },
  },
}

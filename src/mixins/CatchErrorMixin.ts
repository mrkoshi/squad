import Vue from 'vue'
import Component from 'vue-class-component'

@Component
export default class CatchErrorMixin extends Vue {
  protected catchError (error: any) {
    this.$notify.error({
      title: 'Ошибка сервера',
      message: error.response ? error.response.data.message : error.message,
    })
  }
}

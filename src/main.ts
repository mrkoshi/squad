import Vue from 'vue'
import VueImg from 'v-img'
import * as VueGoogleMaps from 'vue2-google-maps'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

import '@/stylesheets/base.scss'

import App from './App.vue'
import router from './router'
import store from './store'

Vue.use(VueImg)
Vue.use(ElementUI)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCSxc1Fp22wmlDz1-ceeBdGXtlQTkCCnCI',
    libraries: ['places', 'visualization'],
  },
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')

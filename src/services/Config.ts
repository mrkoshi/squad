export const baseURL = process.env.NODE_ENV === 'development'
  ? 'http://hackaton.local/api/v1'
  : `/api/v1`

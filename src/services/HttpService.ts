import axios from 'axios'
import qs from 'query-string'

import { baseURL } from '@/services/Config'

const api = axios.create({
  paramsSerializer (params) {
    return qs.stringify(params, { arrayFormat: 'bracket' })
  },
})

api.interceptors.request.use(
  config => {
    if (process.env.NODE_ENV === 'production') {
      config.withCredentials = true
    }

    config.baseURL = baseURL

    return config
  },
  reason => Promise.reject(reason),
)

export default api

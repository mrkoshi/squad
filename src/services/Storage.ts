import { once } from 'lodash/fp'

type Value = any
// tslint:disable-next-line
type IStorage = {
  getItem: (key: string) => Value,
  setItem: (key: string, value: Value) => Value,
  removeItem: (key: string) => void,
}

export const canUseStorage = once(() => {
  const testKey = Math.random().toString(16).slice(2, 8)

  if (typeof (Storage) === 'undefined') {
    return false
  }

  try {
    localStorage.setItem(testKey, 'test')
    localStorage.removeItem(testKey)
    return true
  } catch (error) {
    // tslint:disable-next-line
    console.error(error)
    return false
  }
})

// tslint:disable-next-line
const noop = () => {
}
const fakeStorage: IStorage = {
  getItem: () => undefined,
  removeItem: noop,
  setItem: noop,
}

const storage: IStorage = canUseStorage() ? localStorage : fakeStorage

const addNamespace = (ns: string) => (key: string): string => {
  if (ns === '') {
    return key
  }

  return `${ns}.${key}`
}

function createStorage (namespace: string = '') {
  const withNamespace = addNamespace(namespace)

  if (!canUseStorage()) {
    return {
      setItem (key: string, value: Value) {
        storage.setItem(withNamespace(key), JSON.stringify(value))
      },
      getItem (key: string) {
        const value = storage.getItem(withNamespace(key))

        if (typeof value === 'string') {
          return JSON.parse(value)
        }

        return value
      },
      removeItem (key: string) {
        storage.removeItem(withNamespace(key))
      },
    }
  }

  return localStorage
}

export const selfStorage = createStorage('365_360')

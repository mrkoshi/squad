export interface IOption {
  name: string
  value: string
}

export interface DictionaryState {
  ages: IOption[]
  dates: IOption[]
  genders: IOption[]
}

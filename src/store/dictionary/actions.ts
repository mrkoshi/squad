import { ActionTree } from 'vuex'
import { AxiosResponse } from 'axios'

import api from '@/services/HttpService'
import { RootState } from '@/store/interfaces'
import { IOption, DictionaryState } from '@/store/dictionary/interfaces'

import { DictionaryActions, DictionaryMutations } from './types'

const actions: ActionTree<DictionaryState, RootState> = {
  async [DictionaryActions.A_FETCH_DICTIONARIES] (
    { commit, dispatch },
  ): Promise<IOption[]> {
    dispatch('preloader/A_LOADING', true, { root: true })

    try {
      const response: AxiosResponse<IOption[]> = await api.get(
        '/dictionaries',
      )
      const { data } = response

      commit(DictionaryMutations.M_FETCH_DICTIONARIES, data)
      dispatch('preloader/A_LOADING', false, { root: true })

      return data
    } catch (error) {
      dispatch('preloader/A_LOADING', false, { root: true })

      return Promise.reject(error)
    }
  },
}

// @ts-ignore
export default actions

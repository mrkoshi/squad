import { MutationTree } from 'vuex'

import { DictionaryMutations } from '@/store/dictionary/types'
import { DictionaryState } from '@/store/dictionary/interfaces'

const mutations: MutationTree<DictionaryState> = {
  [DictionaryMutations.M_FETCH_DICTIONARIES] (state, payload: DictionaryState) {
    state.ages = payload.ages
    state.dates = payload.dates
    state.genders = payload.genders
  },
}

export default mutations

export enum DictionaryActions {
  A_FETCH_DICTIONARIES = 'A_FETCH_DICTIONARIES',
}

export enum DictionaryMutations {
  M_FETCH_DICTIONARIES = 'M_FETCH_DICTIONARIES',
}

export enum DictionaryGetters {}

import { Module } from 'vuex'

import { RootState } from '@/store/interfaces'
import { DictionaryState } from '@/store/dictionary/interfaces'

import actions from './actions'
import mutations from './mutations'
import getters from './getters'

export const state: DictionaryState = {
  ages: [],
  dates: [],
  genders: [],
}

const namespaced: boolean = true

export const dictionaryModule: Module<DictionaryState, RootState> = {
  namespaced,
  state,
  actions,
  mutations,
  getters,
}

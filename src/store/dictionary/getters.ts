import { GetterTree } from 'vuex'
import { RootState } from '@/store/interfaces'

import { DictionaryState } from '@/store/dictionary/interfaces'

export default {} as GetterTree<DictionaryState, RootState>

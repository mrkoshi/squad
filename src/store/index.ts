import Vue from 'vue'
import Vuex from 'vuex'

import { dictionaryModule } from '@/store/dictionary'
import { mapModule } from '@/store/map'
import { preloaderModule } from '@components/preloader/store'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  modules: {
    dictionary: dictionaryModule,
    map: mapModule,
    preloader: preloaderModule,
  },
})

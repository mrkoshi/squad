import { ActionTree } from 'vuex'
import { AxiosResponse } from 'axios'

import api from '@/services/HttpService'
import { RootState } from '@/store/interfaces'
import { ICoord, ILocationShort, ILocation, MapState } from '@/store/map/interfaces'

import { MapActions, MapMutations } from './types'

const actions: ActionTree<MapState, RootState> = {
  async [MapActions.A_FETCH_COORDS] (
    { commit, dispatch, state },
  ): Promise<ICoord[]> {
    dispatch('preloader/A_LOADING', true, { root: true })

    try {
      const params = {
        age: state.filter.age || undefined,
        date: state.filter.date || undefined,
        gender: state.filter.gender || undefined,
        location_id: state.location ? state.location.id : undefined,
      }
      const response: AxiosResponse<ICoord[]> = await api.get(
        '/cities/1/coords',
        { params },
      )
      const { data } = response

      commit(MapMutations.M_FETCH_COORDS, data)
      dispatch('preloader/A_LOADING', false, { root: true })

      return data
    } catch (error) {
      dispatch('preloader/A_LOADING', false, { root: true })

      return Promise.reject(error)
    }
  },

  async [MapActions.A_FETCH_LOCATIONS] (
    { commit, dispatch, state },
  ): Promise<ILocationShort[]> {
    dispatch('preloader/A_LOADING', true, { root: true })

    try {
      const params = {
        age: state.filter.age || undefined,
        date: state.filter.date || undefined,
        gender: state.filter.gender || undefined,
      }
      const response: AxiosResponse<ILocationShort[]> = await api.get(
        '/cities/1/locations',
        { params },
      )
      const { data } = response

      commit(MapMutations.M_FETCH_LOCATIONS, data)
      dispatch('preloader/A_LOADING', false, { root: true })

      return data
    } catch (error) {
      dispatch('preloader/A_LOADING', false, { root: true })

      return Promise.reject(error)
    }
  },

  async [MapActions.A_FETCH_LOCATION] (
    { commit, dispatch, state },
    id: number,
  ): Promise<ILocation> {
    dispatch('preloader/A_LOADING', true, { root: true })

    try {
      const params = {
        date: state.filter.date || undefined,
      }
      const response: AxiosResponse<ILocation> = await api.get(
        `/cities/1/locations/${id}`,
        { params },
      )
      const { data } = response

      commit(MapMutations.M_FETCH_LOCATION, data)
      dispatch('preloader/A_LOADING', false, { root: true })

      return data
    } catch (error) {
      dispatch('preloader/A_LOADING', false, { root: true })

      return Promise.reject(error)
    }
  },

  async [MapActions.A_CLEAR_LOCATION] (
    { commit },
  ): Promise<any> {
    await commit(MapMutations.M_CLEAR_LOCATION)
  },
}

// @ts-ignore
export default actions

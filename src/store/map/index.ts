import { Module } from 'vuex'

import { RootState } from '@/store/interfaces'
import { MapState } from '@/store/map/interfaces'

import actions from './actions'
import mutations from './mutations'
import getters from './getters'

export const state: MapState = {
  filter: {
    age: null,
    date: null,
    gender: null,
  },
  coords: [],
  location: undefined,
  locations: [],
}

const namespaced: boolean = true

export const mapModule: Module<MapState, RootState> = {
  namespaced,
  state,
  actions,
  mutations,
  getters,
}

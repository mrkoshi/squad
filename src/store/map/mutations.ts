import { MutationTree } from 'vuex'

import { MapMutations } from '@/store/map/types'
import { MapState, ICoord, ILocation, ILocationShort } from '@/store/map/interfaces'

const mutations: MutationTree<MapState> = {
  [MapMutations.M_FETCH_COORDS] (state, payload: ICoord[]) {
    state.coords = payload
  },

  [MapMutations.M_FETCH_LOCATIONS] (state, payload: ILocationShort[]) {
    state.locations = payload
  },

  [MapMutations.M_FETCH_LOCATION] (state, payload: ILocation) {
    state.location = payload
  },

  [MapMutations.M_CLEAR_LOCATION] (state) {
    state.location = undefined
  },

  [MapMutations.M_SET_DATE] (state, payload: string) {
    state.filter = Object.assign({}, state.filter, { date: payload })
  },

  [MapMutations.M_SET_AGE] (state, payload: string) {
    state.filter = Object.assign({}, state.filter, { age: payload })
  },

  [MapMutations.M_SET_GENDER] (state, payload: string) {
    state.filter = Object.assign({}, state.filter, { gender: payload })
  },
}

export default mutations

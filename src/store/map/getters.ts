import { GetterTree } from 'vuex'
import { RootState } from '@/store/interfaces'

import { MapState } from '@/store/map/interfaces'

export default {} as GetterTree<MapState, RootState>

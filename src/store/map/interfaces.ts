import { IOption } from '@store/dictionary/interfaces'

export interface IPosition {
  lat: number
  lng: number
}

export interface ICoord {
  id: number
  location: IPosition
  weight: number
}

export interface ILocationShort {
  age: string
  created_at: string
  id: 1
  points_count: number
  position: IPosition
  preview: string
  title: string
}

export interface IImage {
  preview: string
  url: string
}

export interface ILocation extends ILocationShort {
  address: string
  ages: IOption[]
  genders: IOption[]
  points: IImage[]
  with_children: number
  tourists: number
}

export interface IFilter {
  age: string | null
  date: string | null
  gender: string | null
}

export interface MapState {
  filter: IFilter
  coords: ICoord[]
  location?: ILocation
  locations: ILocationShort[]
}

import { Module } from 'vuex'
import { namespace } from 'vuex-class'

import { RootState } from '@store/interfaces'

import actions from './actions'
import mutations from './mutations'
import getters from './getters'

export const Preloader = namespace('preloader')

export interface IPreloader {
  loading: boolean
}

export const state: IPreloader = {
  loading: false,
}

const namespaced: boolean = true

export const preloaderModule: Module<IPreloader, RootState> = {
  namespaced,
  state,
  actions,
  mutations,
  getters,
}

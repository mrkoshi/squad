import { MutationTree } from 'vuex'
import { remove } from 'lodash/fp'

import { PreloaderMutations } from './types'
import { IPreloader } from './'

const mutations: MutationTree<IPreloader> = {
  [PreloaderMutations.M_LOADING] (state, payload: boolean) {
    state.loading = payload
  },
}

export default mutations

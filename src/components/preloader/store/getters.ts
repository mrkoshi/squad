import { GetterTree } from 'vuex'
import { RootState } from '@store/interfaces'

import { IPreloader } from './'

export default {} as GetterTree<IPreloader, RootState>

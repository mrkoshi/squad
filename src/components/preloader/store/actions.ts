import { ActionTree } from 'vuex'
import { RootState } from '@store/interfaces'

import { PreloaderActions, PreloaderMutations } from './types'
import { IPreloader } from './'

const actions: ActionTree<IPreloader, RootState> = {
  async [PreloaderActions.A_LOADING] (
    { commit },
    payload: string,
  ): Promise<any> {
    await commit(PreloaderMutations.M_LOADING, payload)
  },
}

// @ts-ignore
export default actions

export enum PreloaderActions {
  A_LOADING = 'A_LOADING',
}

export enum PreloaderMutations {
  M_LOADING = 'M_LOADING',
}
